<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="../../public/css/app.css" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .container{
                margin-top: 100px;
            }
            .fluid{
                margin-left:auto;
                margin-right:auto;
                background-color: rgba(77,58,18,0.3);
                width: 980px;
                height: 1200px;

            }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="fluid">
                <section class="deiscription">
                    <div class="big-title">
                        <span class="big-title__span">ネイルサロン予約ページ</span>
                    </div>
                    <div class="big-description">
                        <span class="big-description__span">
                    </div>
                </section>
                <section class="callender">
                    <div class="pagenation">
                    </div>
                    <div class="currentDate">
                    </div>
                    <table class="callender__table">
                    </table>
                </section>
            </div>
        </div>
    </body>
</html>
